Introduction
------------
This repository contains the Linaro Trustedsubstrate layers for OpenEmbedded.

Building
--------

Make sure kas is installed on your system
```kas build ci/<platform.yaml>```

Supported platforms:
- qemuarm64-secureboot
- qemuarm-secureboot
- synquacer
- stm32mp157c-dk2
- stm32mp157c-ev1
- rockpi4b
- rpi4
- zynqmp-kria-starter

For more information on using the OpenEmbedded layer look at 
https://trs.readthedocs.io/en/latest/firmware/index.html

CI
--

Latest daily build:
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge" />
        </a>

<!--
  Gitlab allows certain HTML tags to be added as Markdown.
  The full list of allowed tags is here: https://github.com/gjtorikian/html-pipeline/blob/a363d620eba0076479faad86f0cf85a56b2b3c0a/lib/html/pipeline/sanitization_filter.rb
  Also note that there cannot be spaces between tags, otherwise HTML parser doesn't work
-->

<table style="width:100%">
  <thead>
    <th></th>
    <th>QEMU</th>
    <th>Rockpi4b</th>
    <th>RPI4</th>
    <th>stm32mp157c-dk2</th>
    <th>stm32mp157c-ev1</th>
    <th>SynQuacer</th>
    <th>Xilinx kv260 starter kit</th>
  <thead>
  <tbody>
    <!-- build suite -->
    <tr>
      <th>build</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rockpi4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rockpi4b&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rpi4&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- stm32mp157c-dk2 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32mp157c-dk2&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- stm32mp157c-ev1 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32mp157c-ev1&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zynqmp-starter -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zynqmp-starter&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of build suite -->
    <!-- boot suite (smoke-ls) -->
    <tr>
      <th>boot</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- stm32mp157c-dk2 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32mp157c-dk2&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- stm32mp157c-ev1 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=smoke-ls&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of boot suite -->
    <!-- measured-boot suite -->
    <tr>
      <th>measured boot</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=measured-boot&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=measured-boot&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- stm32mp157c-dk2 -->
      <td>
        N/A
      </td>
      <!-- stm32mp157c-ev1 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=measured-boot&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        TBD
      </td>
    </tr>
    <!-- end of measured-boot suite -->
    <!-- xtest suite -->
    <tr>
      <th>optee-xtest</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- stm32mp157c-dk2 -->
      <td>
        TBD
      </td>
      <!-- stm32mp157c-ev1 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img height="20" width="70" src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=xtest&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of xtest suite -->
    <!-- secure-boot suite -->
    <tr>
      <th>secure boot</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rpi4 -->
      <td>
        N/A
      </td>
      <!-- stm32mp157c-dk2 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=stm32mp157c-dk2&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- stm32mp157c-ev1 -->
      <td>
        N/A
      </td>
      <!-- synquacer -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=synquacer&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=secure-boot-enabled&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of secure-boot suite -->
  </tbody>
</table>


Images
------
  * Download images:
    * qemu
        * [flash.bin-qemu.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/flash.bin-qemu.gz?job=build-meta-ts-qemuarm64-secureboot)
    * rockpi
        * [ts-firmware-rockpi4b.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rockpi4b.rootfs.wic.gz?job=build-meta-ts-rockpi4b)
    * raspberry pi
        * [ts-firmware-rpi4.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rpi4.rootfs.wic.gz?job=build-meta-ts-rpi4)
    * stm32mp157c dk2
        * [ts-firmware-stm32mp157c-dk2.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-stm32mp157c-dk2.rootfs.wic.gz?job=build-meta-ts-stm32mp157c-dk2)
    * stm32mp157c-ev1
        * [ts-firmware-stm32mp157c-ev1.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-stm32mp157c-ev1.rootfs.wic.gz?job=build-meta-ts-stm32mp157c-ev1)
    * synquacer
        * [fip.bin-synquacer.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/fip.bin-synquacer.gz?job=build-meta-ts-synquacer)
        * [scp_romramfw_release.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/scp_romramfw_release.bin.gz?job=build-meta-ts-synquacer)
    * zynqmp-starter
        * [ImageA.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageA.bin.gz?job=build-meta-ts-zynqmp-starter)
        * [ImageB.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageB.bin.gz?job=build-meta-ts-zynqmp-starter)
