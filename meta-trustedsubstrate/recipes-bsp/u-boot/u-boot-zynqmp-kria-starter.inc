# Generate zynqmp-kria-starter kit style loader binaries
inherit deploy

SRC_URI += "file://xilinx_zynqmp_kria_starter_defconfig"
SRC_URI += "file://pmu_obj.bin"
SRC_URI += "file://pmufw.bin"
SRC_URI += "file://zynqmp_fsbl.elf"
SRC_URI += "file://pmufw.elf"
SRC_URI += "file://som.its"
SRC_URI += "file://0001-som-Kria-specific-setting-configurations.patch"
SRC_URI += "file://0001-arm64-zynqmp-Add-an-OP-TEE-node-to-the-device-tree.patch"
SRC_URI += "file://0001-u-boot-zynqmp-Disable-the-hub-node-from-the-usb-nide.patch"
SRC_URI += "file://primary.pem"
SRC_URI += "file://secondary.pem"

COMPATIBLE_MACHINE = "zynqmp-kria-starter"

DEPENDS += "trusted-firmware-a optee-os"
DEPENDS += "xxd-native"
DEPENDS += "u-boot-mkimage-native bootgen-native"

UBOOT_BOARDDIR = "${S}/board/xilinx/zynqmp"
UBOOT_ENV_NAME = "zynqmp.env"

do_compile:prepend() {
    cp ${S}/../pmufw.bin ${S}/
    cp ${S}/../pmufw.bin ${B}/xilinx_zynqmp_kria_starter_defconfig/
    cp ${S}/../pmu_obj.bin ${S}/
    cp ${S}/../pmu_obj.bin ${B}/xilinx_zynqmp_kria_starter_defconfig/
    cp ${S}/../pmufw.elf ${B}/xilinx_zynqmp_kria_starter_defconfig/
    cp ${S}/../zynqmp_fsbl.elf ${B}/xilinx_zynqmp_kria_starter_defconfig/
    cp ${S}/som.its ${B}/xilinx_zynqmp_kria_starter_defconfig/
    cp ${RECIPE_SYSROOT}/firmware/bl31.elf ${B}/xilinx_zynqmp_kria_starter_defconfig/
    # FIXME we need to use tee-raw.bin, change this once https://github.com/OP-TEE/optee_os/pull/5830 gets merged
    cp ${RECIPE_SYSROOT}/lib/firmware/tee-pager_v2.bin ${B}/xilinx_zynqmp_kria_starter_defconfig/
    cp ${S}/../*.pem ${B}/xilinx_zynqmp_kria_starter_defconfig/
}

do_deploy:append() {
    mkdir -p ${DEPLOYDIR}
    cd ${B}/xilinx_zynqmp_kria_starter_defconfig/

    fdtoverlay -o zynqmp-smk-k26-revA-sck-kv-g-revA.dtb -i arch/arm/dts/zynqmp-smk-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revA.dtbo
    fdtoverlay -o zynqmp-smk-k26-revA-sck-kv-g-revB.dtb -i arch/arm/dts/zynqmp-smk-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revB.dtbo
    fdtoverlay -o zynqmp-sm-k26-revA-sck-kv-g-revA.dtb -i arch/arm/dts/zynqmp-sm-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revA.dtbo
    fdtoverlay -o zynqmp-sm-k26-revA-sck-kv-g-revB.dtb -i arch/arm/dts/zynqmp-sm-k26-revA.dtb arch/arm/dts/zynqmp-sck-kv-g-revB.dtbo
    # Pack all combinations together
    mkimage -E -f som.its -B 0x8 fit-dtb.blob

    # Not needed, but keeping it for reference.  The generated sha3.txt
    # is what needs to be burned in the eFUSE for production use
    cat <<EOF > hash.bif
    generate_hash_ppk:
    {
    [pskfile] primary.pem
    [sskfile] secondary.pem
     [ bootloader,
       destination_cpu = a53-0,
       authentication = rsa
     ] zynqmp_fsbl.elf
    }
EOF

    bootgen -image hash.bif -arch zynqmp -w -o output_image.bin -efuseppkbits sha3.txt

    cat <<EOF > bootgen.bif
    the_ROM_image:
    {
    [fsbl_config] bh_auth_enable
    [auth_params] ppk_select=0
    [pskfile] primary.pem
    [sskfile] secondary.pem

    [ bootloader,
      destination_cpu=a53-0,
      authentication=rsa
    ] zynqmp_fsbl.elf
    [ destination_cpu=a53-0,
      exception_level=el-3,
      trustzone=secure,
      authentication=rsa
    ] bl31.elf
    [ destination_cpu = pmu,
     authentication = rsa
    ] pmufw.elf
    [ destination_cpu=a53-0,
      load=0x00100000,
      authentication=rsa
    ] fit-dtb.blob
    [ destination_cpu=a53-0,
      exception_level=el-2,
      authentication=rsa
    ] u-boot.elf
    [ destination_cpu=a53-0,
      load=0x60000000,
      startup=0x60000000,
      exception_level=el-1,
      trustzone=secure,
      authentication=rsa
    ] tee-pager_v2.bin
    }
EOF

    bootgen -image bootgen.bif -arch zynqmp -r -w -o xilinx_boot.bin

    cd -
    cp ${B}/xilinx_zynqmp_kria_starter_defconfig/xilinx_boot.bin ${DEPLOYDIR}/ImageA.bin
    cp ${B}/xilinx_zynqmp_kria_starter_defconfig/xilinx_boot.bin ${DEPLOYDIR}/ImageB.bin
}

ATF_DEPENDS = " trusted-firmware-a:do_deploy"
do_compile[depends] .= "${ATF_DEPENDS}"
