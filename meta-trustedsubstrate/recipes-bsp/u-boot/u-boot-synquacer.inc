FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/synquacer:"

SRC_URI += "file://synquacer_developerbox_defconfig"
SRC_URI += "file://0001-synquacer-support-new-NOR-Flash-firmware-layout.patch"

UBOOT_BOARDDIR = "${S}/board/socionext/developerbox"
UBOOT_ENV_NAME = "developerbox.env"
