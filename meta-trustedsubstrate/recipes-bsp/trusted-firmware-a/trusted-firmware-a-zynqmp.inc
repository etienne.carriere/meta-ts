# Xilinx kv260 and SOMs

COMPATIBLE_MACHINE:zynqmp-kria-starter = "zynqmp-kria-starter"

PV .= "+git${SRCREV_tfa}"

FILESEXTRAPATHS:prepend:zynqmp-kria-starter := "${THISDIR}/files/zynqmp-kria-starter:"

TFA_DEBUG = "0"
TFA_UBOOT = "0"
#TFA_MBEDTLS = "1"
TFA_BUILD_TARGET = "bl31"
TFA_INSTALL_TARGET = "bl31"
# Enabling Secure-EL1 Payload Dispatcher (SPD)
TFA_SPD = "opteed"

TFA_TARGET_PLATFORM = "zynqmp"

EXTRA_OEMAKE += " ZYNQMP_CONSOLE=cadence1 ZYNQMP_ATF_MEM_BASE=0x50000000 \
		    ZYNQMP_ATF_MEM_SIZE=0x80000 RESET_TO_BL31=1 NEED_BL32=1"

do_deploy:append() {
    cp ${D}/firmware/bl31.bin ${DEPLOYDIR}
}

addtask deploy before do_build after do_compile
